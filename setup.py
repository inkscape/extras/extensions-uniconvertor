#!/usr/bin/env python

from setuptools import setup
from inkman.utils import get_description

setup(
    name='inkscape-uniconvertor',
    version='1.1.5',
    description='Allow saving and loading of many different vector formats using uniconvertor',
    long_description=get_description(__file__),

    url='https://sk1project.net/',
    author='Stephen Silver',
    author_email='',
    platforms='linux',
    license='GPLv2',

    # Make sure to include all the inx files, place the in the inx directory
    data_files=[('uniconv', [
        'ai_input.inx',
        'ccx_input.inx',
        'cdr_input.inx',
        'cdt_input.inx',
        'cgm_input.inx',
        'cmx_input.inx',
        'plt_input.inx',
        'plt_output.inx',
        'plt_output.py',
        'sk1_input.inx',
        'sk1_output.inx',
        'sk1_output.py',
        'uniconv-ext.py',
        'uniconv_output.py',
        'wmf_input.inx',
        'wmf_output.inx',
        'wmf_output.py',
    ])],

    # Include any of the extension scripts, remember they will end up in the bin directory
    #scripts=[],

    # Optionally include packages (if your extension includes modules) make sure
    # that include package data is True if you have data files in your modules
    #packages=[],
    include_package_data=True,

    classifiers=[
        # These are REQUIRED for this to be detected as a valid inkscape extension
        'Environment :: Plugins',
        'Topic :: Multimedia :: Graphics :: Editors :: Vector-Based',

        # These are optional and depend on your requirements
        'Intended Audience :: Other Audience',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
